
import {
  MessageBody as MsgBody, OnGatewayConnection, OnGatewayDisconnect, OnGatewayInit,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
  WsResponse,
} from '@nestjs/websockets';
import { from, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Server } from 'socket.io';
import { MessageBody } from '../bodies/message-body';

@WebSocketGateway(3001)
export class EventsGateway implements OnGatewayConnection, OnGatewayInit, OnGatewayDisconnect{
  @WebSocketServer()
  server: Server;
  constructor() {
  }
  @SubscribeMessage('test')
  testmsg(@MsgBody() body: string): void
  {
    console.log('Message received: ' + body);
  }
  /*
  // Code de test de la doc
  @SubscribeMessage('events')
  findAll(@MsgBody() data: any): Observable<WsResponse<number>> {
    return from([1, 2, 3]).pipe(map(item => ({ event: 'events', data: item })));
  }

  @SubscribeMessage('identity')
  async identity(@MsgBody() data: number): Promise<number> {
    return data;
  }*/
  public sendMessage(msg: MessageBody): void {
    this.server.emit('message', msg);
  }
  public closeChannel(id: number): void {
    this.server.emit('close', [id]);
  }
  afterInit(server: any): any {
    this.server.emit('test', ['test from server !']);
  }

  handleConnection(client: any, ...args: any[]): any {
  }

  handleDisconnect(client: any): any {
  }
}
