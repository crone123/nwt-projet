import {IsString, IsNotEmpty} from 'class-validator';
import { Message } from '../schema/message';
import { ApiProperty } from '@nestjs/swagger';

export class MessageBody
{
  @ApiProperty({
    name: 'id',
    description: 'Message ID',
    example: 1,
  })
  id?: number;
  @ApiProperty({
    name: 'pseudo',
    description: 'User name',
    example: 'John',
  })
  @IsString()
  @IsNotEmpty()
  pseudo: string;
  @ApiProperty({
    name: 'message',
    description: 'Message content',
    example: 'Hello world',
  })
  @IsString()
  @IsNotEmpty()
  message: string;
  @ApiProperty({
    name: 'channel',
    description: 'Channel ID',
    example: 1,
  })
  channel: number;
  @ApiProperty({
    name: 'timestamp',
    description: 'Timestamp, can be empty, set by the server.',
    example: 123456789,
  })
  timestamp?: number;
  @ApiProperty({
    name: 'likes',
    description: 'Array containing like username',
    example: ['John', 'Doe', 'Mario', 'Luigi'],
  })
  likes?: Array<string>;

  from(m: Message): void
  {
    this.id = m.id;
    this.pseudo = m.pseudo;
    this.message = m.message;
    this.channel = m.channel;
    this.timestamp = m.timestamp;
    this.likes = m.likes;
  }
}
