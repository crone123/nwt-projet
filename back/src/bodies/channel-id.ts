import {IsString, IsNotEmpty} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ChannelId
{
  @ApiProperty({
    name: 'id',
    description: 'Channel ID',
    example: 1,
  })
  id: number;
}
