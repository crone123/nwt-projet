import {IsString, IsNotEmpty} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ChannelBody
{
  @ApiProperty({
    name: 'id',
    description: 'Channel ID',
    example: 1,
  })
  id?: number;
  @ApiProperty({
    name: 'name',
    description: 'Channel name',
    example: 'My Channel'
  })
  @IsString()
  @IsNotEmpty()
  name: string;
}
