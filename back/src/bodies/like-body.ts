import {IsString, IsNotEmpty} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class LikeBody
{

  @ApiProperty({
    name: 'id',
    description: 'Message ID',
    example: 1,
  })
  @IsNotEmpty()
  idMessage: number;

  @ApiProperty({
    name: 'pseudo',
    description: 'User name for a like',
    example: 'John',
  })
  @IsString()
  @IsNotEmpty()
  pseudo: string;
}
