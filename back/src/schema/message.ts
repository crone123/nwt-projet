import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { Channel } from './channel';
import { ApiProperty } from '@nestjs/swagger';

@Schema({versionKey: false, toJSON: {
    transform(doc, ret) {
      delete ret._id;
    }
  }})
export class Message extends mongoose.Document
{
  @ApiProperty({
    name: 'id',
    description: 'Message ID',
    example: 1,
  })
  @Prop({required: true, type: Number})
  id: number;
  @ApiProperty({
    name: 'pseudo',
    description: 'User name',
    example: 'John',
  })
  @Prop({required: true, type: String, trim: true})
  pseudo: string;
  @ApiProperty({
    name: 'message',
    description: 'Message content',
    example: 'Hello world',
  })
  @Prop({required: true, type: String, trim: true})
  message: string;
  @ApiProperty({
    name: 'channel',
    description: 'Channel ID',
    example: 1,
  })
  @Prop({required: true, type: Set})
  likes: Array<string>;
  @ApiProperty({
    name: 'timestamp',
    description: 'Timestamp, can be empty, set by the server.',
    example: 123456789,
  })
  @Prop({required: true, type: Number})
  timestamp: number;
  @ApiProperty({
    name: 'likes',
    description: 'Array containing like username',
    example: ['John', 'Doe', 'Mario', 'Luigi'],
  })
  @Prop({required: true, type: Number})
  channel: number;
}
export const MessageSchema = SchemaFactory.createForClass(Message);
