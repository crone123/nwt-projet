import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { ApiProperty } from '@nestjs/swagger';

@Schema({versionKey: false, toJSON: {
    transform(doc, ret) {
      delete ret._id;
    }
  }})
export class Channel extends mongoose.Document
{
  @ApiProperty({
    name: 'id',
    description: 'Channel ID',
    example: 1,
  })
  @Prop({required: true, type: Number})
  id: number;
  @ApiProperty({
    name: 'name',
    description: 'Channel name',
    example: 'My Channel'
  })
  @Prop({required: true, type: String, trim: true})
  name: string;
}
export const ChannelSchema = SchemaFactory.createForClass(Channel);
