import { InjectModel } from '@nestjs/mongoose';
import { Message } from '../schema/message';
import { Model, MongooseDocument } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { from, Observable } from 'rxjs';
import { Channel } from '../schema/channel';
import { map, mergeMap } from 'rxjs/operators';
import { ChannelBody } from '../bodies/channel-body';
import { MessageBody } from '../bodies/message-body';
import { doc } from 'prettier';
import { LikeBody } from '../bodies/like-body';

@Injectable()
export class MessageDAO
{
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  constructor(@InjectModel(Message.name) private messageModel: Model<Message>) {
  }
  findByChannel(channel: number): Observable<Message[]> {
    return from(this.messageModel.find({channel: channel}).sort({timestamp: 1}));
  }
  findById(id: number): Observable<Message> {
    return from(this.messageModel.findOne({id: id}));
  }
  insertOne(channel: MessageBody): Observable<boolean> {
    //console.log('Channel: ' + channel.id + ', ' + channel.name);
    return this.findById(channel.id).pipe(mergeMap(
      obj =>
      {
        if (obj == null)
        {
          return from(this.messageModel.insertMany([channel])).pipe(map(
            obj => {
              return true;
            }
          ));
        }
        else
        {
          return from([false]);
        }
      }
    ));
  }
  lastId(): Observable<number>
  {
    return from(this.messageModel.aggregate([{$group: {_id: 0, maxid: {$max: "$id"}}}])).pipe(
      map(obj =>
        {
          if (obj.length > 0)
          {
            return obj[0].maxid;
          }
          else
          {
            return 0;
          }
        }
      )
    );
  }

  like(like : LikeBody): Observable<boolean> {

    return this.findById(like.idMessage).pipe(mergeMap(
      (obj : Message) =>
      {
        //console.log(obj)
        if(obj!=null){
          return from(this.messageModel.updateOne({id: obj.id},{$addToSet: { likes: like.pseudo} })).pipe(
            map(obj=>{
              return true;
          }))
        }else{
          return from([false]);
        }
      }
    ));
  }
  disLike(like : LikeBody): Observable<boolean> {
    return this.findById(like.idMessage).pipe(mergeMap(
      obj=>
      {
        if(obj!=null){
          return from(this.messageModel.updateOne({id: obj.id},{$pull:{likes: like.pseudo}})).pipe(
            map(obj=>{
              return true;
            }))
        }else{
          return from([false]);
        }
      }
    ));
  }
  updateMessage(msg : MessageBody):Observable<boolean>{
    return this.findById(msg.id).pipe(mergeMap(
      obj=>
      {
        if(obj!=null){
          return from(this.messageModel.updateOne({id: obj.id},{$set:{message:msg.message}})).pipe(
            map(obj=>{
              return true;
            }))
        }else{
          return from([false]);
        }
      }
    ));

  }
  deleteChannel(channelId: number): Observable<boolean> {
    return from(this.messageModel.deleteMany({channel: channelId})).pipe(mergeMap(_ => {
      return from([true]);
    }));
  }
}
