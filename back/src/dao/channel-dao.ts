import { InjectModel } from '@nestjs/mongoose';
import { Channel } from '../schema/channel';
import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { from, Observable } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
import { ChannelBody } from '../bodies/channel-body';

@Injectable()
export class ChannelDAO
{
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  constructor(@InjectModel(Channel.name) private channelModel: Model<Channel>) {

  }
  find(): Observable<Channel[]> {
    return from(this.channelModel.find().sort({id: -1}));
  }

  findById(id: number): Observable<Channel> {
    return from(this.channelModel.findOne({id: id}));
  }
  insertOne(channel: ChannelBody): Observable<boolean> {
    //console.log('Channel: ' + channel.id + ', ' + channel.name);
    return this.findById(channel.id).pipe(mergeMap(
      obj =>
      {
        if (obj == null)
        {
          return from(this.channelModel.insertMany([channel])).pipe(map(
            obj => {
              return true;
            }
          ));
        }
        else
        {
          return from([false]);
        }
      }
    ));
  }
  lastId(): Observable<number>
  {
    return from(this.channelModel.aggregate([{$group: {_id: 0, maxid: {$max: "$id"}}}])).pipe(
      map(obj =>
        {
          if (obj.length > 0)
          {
           return obj[0].maxid;
          }
          else
          {
           return 0;
          }
        }
      )
    );
  }
  deleteChannel(channelId: number): Observable<boolean> {
    return from(this.channelModel.deleteOne({id: channelId})).pipe(mergeMap(_ => {
      return from([true]);
    }));
  }
}
