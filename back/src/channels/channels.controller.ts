import { Body, Controller, Get, Header, Param, Post } from '@nestjs/common';
import { from, Observable } from 'rxjs';
import { Channel } from '../schema/channel';
import { ChannelDAO } from '../dao/channel-dao';
import { ChannelBody } from '../bodies/channel-body';
import { map, mergeMap } from 'rxjs/operators';
import { MessageDAO } from '../dao/message-dao';
import { ChannelId } from '../bodies/channel-id';
import { EventsGateway } from '../events/event-gateway';
import { ApiOkResponse, ApiParam } from '@nestjs/swagger';
@Controller('channels')
export class ChannelsController
{
  constructor(private dao: ChannelDAO, private mdao: MessageDAO, private eg: EventsGateway) {

  }
  @ApiOkResponse({description: 'Return the channel list', type: Channel})
  @Get('/list')
  @Header('Content-Type', 'application/json')
  list(): Observable<Channel[]>
  {
    return this.dao.find();
  }

  @ApiParam({name: 'id', description: 'Channel ID', example: 1})
  @ApiOkResponse({description: 'Get the channel by id', type: Channel})
  @Get('/get/:id')
  @Header('Content-Type', 'application/json')
  get(@Param('id') id: number): Observable<Channel>
  {
      return this.dao.findById(id);
  }

  @ApiOkResponse({description: 'Get the last message id', type: Number})
  @Get('/last')
  last(): Observable<number>
  {
    return this.dao.lastId();
  }
  @ApiOkResponse({description: 'Create a new channel', type: Boolean})
  @Post('/create')
  @Header('Content-Type', 'application/json')
  create(@Body() body: ChannelBody): Observable<boolean>
  {
    return this.dao.lastId().pipe(mergeMap(id => {
      body.id = id + 1;
      return this.dao.insertOne(body);
    }));
  }
  @ApiOkResponse({description: 'Remove a channel', type: Boolean})
  @Post('/remove')
  @Header('Content-Type', 'application/json')
  remove(@Body() body: ChannelId): Observable<boolean>
  {
    this.eg.closeChannel(body.id);
    return this.dao.deleteChannel(body.id).pipe(mergeMap(
      _ => {
        return this.mdao.deleteChannel(body.id).pipe(mergeMap(_ => {
          return from([true]);
        }))
      })
    );
  }

}
