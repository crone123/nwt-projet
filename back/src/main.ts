import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();

  const appOptions = new DocumentBuilder()
    .setTitle('Angular Messenger API')
    .setDescription('API for Angular Messenger')
    .setVersion('1.0.0')
    .addTag('api')
    .build();

  const appDocument = SwaggerModule.createDocument(app, appOptions, {include: [AppModule]});
  SwaggerModule.setup('/swagger/', app, appDocument);
  await app.listen(3000);
}
bootstrap();
