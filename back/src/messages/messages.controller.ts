import { Body, Controller, Get, Header, Param, Post } from '@nestjs/common';
import { from, Observable } from 'rxjs';
import { Message } from '../schema/message';
import { MessageDAO } from '../dao/message-dao';
import { MessageBody } from '../bodies/message-body';
import { ChannelDAO } from '../dao/channel-dao';
import { map, mergeMap } from 'rxjs/operators';
import { EventsGateway } from '../events/event-gateway';
import { LikeBody } from '../bodies/like-body';
import { ApiOkResponse, ApiParam } from '@nestjs/swagger';

@Controller('messages')
export class MessagesController
{
  constructor(private dao: MessageDAO, private channeldao: ChannelDAO, private eg: EventsGateway) {
  }
  @ApiParam({name: 'channel', description: 'Channel ID', example: 1})
  @ApiOkResponse({description: 'Get the messages from the channel', type: Message})
  @Get('/list/:channel')
  @Header('Content-Type', 'application/json')
  getByChannel(@Param('channel') channel: number): Observable<Message[]>
  {
    return this.dao.findByChannel(channel);
  }
  @ApiOkResponse({description: 'Send a message to a channel', type: Boolean})
  @Post('/send')
  @Header('Content-Type', 'application/json')
  sendMessage(@Body() body: MessageBody): Observable<boolean>
  {
    body.timestamp = new Date().getTime();
    body.likes = new Array<string>();
    return from(this.channeldao.findById(body.channel).pipe(
      mergeMap(obj =>
      {
        if (obj == null)
        {
          return from([false]);
        }
        else
        {
          return from(this.dao.lastId()).pipe(
            mergeMap(
              id =>
              {
                body.id = id + 1;
                return from(this.dao.insertOne(body)).pipe(
                  mergeMap(
                    val => {
                      if (val === true)
                      {
                        this.eg.sendMessage(body);
                        return from([true]);
                      }
                      else
                      {
                        return from([false]);
                      }
                    }
                  )
                );
              }
            )
          );
        }
      })
    ));
  }

  @ApiOkResponse({description: 'Like a message', type: Boolean})
  @Post('/like')
  @Header('Content-Type','application/json')
  likeMessage(@Body() like: LikeBody):Observable<boolean>{

    return this.dao.like(like)
      .pipe(
      mergeMap(
        val => {
          if (val === true) {
            return from(this.dao.findById(like.idMessage)).pipe(mergeMap( message =>
            {
              const mb = new MessageBody();
              mb.from(message);
              this.eg.sendMessage(mb);
              return from([true]);
            }));
          } else {
            return from([false]);
          }
        }
        )
    );
  }

  @ApiOkResponse({description: 'Dislike a message', type: Boolean})
  @Post('/dislike')
  @Header('Content-Type','application/json')
  disLikeMessage(@Body() like: LikeBody):Observable<boolean>{
    return this.dao.disLike(like).pipe(
      mergeMap(
        val => {
          if (val === true) {
            return from(this.dao.findById(like.idMessage)).pipe(mergeMap( message =>
            {
              const mb = new MessageBody();
              mb.from(message);
              this.eg.sendMessage(mb);
              return from([true]);
            }));
          } else {
            return from([false]);
          }
        }
      )
    );
  }

  @ApiOkResponse({description: 'Update the content of a message', type: Boolean})
  @Post('/update')
  @Header('Content-Type','application/json')
  updateMessage(@Body() msg:MessageBody):Observable<boolean>{
    return this.dao.updateMessage(msg).pipe(mergeMap(
      value => {
        if (value == true)
        {
          return from(this.dao.findById(msg.id)).pipe(mergeMap(dbmsg => {
            const newmsg = new MessageBody();
            newmsg.from(dbmsg);
            this.eg.sendMessage(newmsg);
            return from([true]);
          }));
        }
        else
        {
          return from([false]);
        }
      }
    ));
  }
}
