import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ChannelsController } from './channels/channels.controller';
import { MessagesController } from './messages/messages.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Channel, ChannelSchema } from './schema/channel';
import { Message, MessageSchema } from './schema/message';
import { ChannelDAO } from './dao/channel-dao';
import { MessageDAO } from './dao/message-dao';
import { EventsGateway } from './events/event-gateway';


@Module({
  imports: [MongooseModule.forRoot('mongodb://localhost/nwt-projet'),
  MongooseModule.forFeature([
    {name: Channel.name, schema: ChannelSchema},
    {name: Message.name, schema: MessageSchema}
    ])],
  controllers: [AppController, ChannelsController, MessagesController],
  providers: [AppService, ChannelDAO, MessageDAO, EventsGateway],
})
export class AppModule {}
