import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello World! <a href="/swagger/" >Go to the swagger</a>';
  }
}
