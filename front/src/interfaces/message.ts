export interface Message
{
  id: number;
  likes: string[];
  pseudo: string;
  message: string;
  timestamp: number;
  channel: number;
}
