import {Component, EventEmitter, Inject, OnInit} from '@angular/core';
import {ChannelItem} from '../../interfaces/channel-item';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {HttpClient, HttpParams} from '@angular/common/http';
import {BACK_HTTP} from "../global";
import {AlertDialogComponent} from "../alert-dialog/alert-dialog.component";

@Component({
  selector: 'app-remove-channel-dialog',
  templateUrl: './remove-channel-dialog.component.html',
  styleUrls: ['./remove-channel-dialog.component.css']
})
export class RemoveChannelDialogComponent implements OnInit {

  channel: ChannelItem;
  removeSuccess: EventEmitter<any>;
  constructor(private dialogRef: MatDialogRef<RemoveChannelDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: ChannelItem, private client: HttpClient, public alert: MatDialog) {
    this.channel = data;
    this.removeSuccess = new EventEmitter<any>();
  }

  ngOnInit(): void {

  }
  cancel(): void {
    this.dialogRef.close();
  }
  remove(): void {
    this.client.post<boolean>(BACK_HTTP + '/channels/remove', new HttpParams().append('id', String(this.channel.id))).subscribe(
      req => {
        console.log(req);
        if (req === true)
        {
          this.removeSuccess.emit();
          this.dialogRef.close();
        }
        else
        {
          const dialogRef = this.alert.open(AlertDialogComponent, {data: {title: 'Erreur de suppression de salon', message: 'Le serveur à refusé la demande.'}});
        }
      }
    );
  }
}
