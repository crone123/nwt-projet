import {Component, EventEmitter, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material/dialog';
import {RemoveChannelDialogComponent} from '../remove-channel-dialog/remove-channel-dialog.component';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-channel-item',
  templateUrl: './channel-item.component.html',
  styleUrls: ['./channel-item.component.css']
})
export class ChannelItemComponent implements OnInit {
  @Input()
  id: number;
  @Input()
  name: string;
  @Input()
  refreshEmitter: EventEmitter<any>;
  sub: Subscription;
  constructor(private router: Router, public dialog: MatDialog) { }

  ngOnInit(): void {
  }

  openChannel(): void {
    this.router.navigate(['/channel/' + this.id]);
  }

  deleteChannel(): void {
    const dialogRef = this.dialog.open(RemoveChannelDialogComponent, {data: {id: this.id, name: this.name}});
    this.sub = dialogRef.componentInstance.removeSuccess.subscribe(_ => {
      this.refreshEmitter.emit();
    });
    dialogRef.afterClosed().subscribe(_ => {
      this.sub.unsubscribe();
      this.sub = null;
    });
  }
}
