import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

export interface MessageData
{
  title: string;
  message: string;
}

@Component({
  selector: 'app-alert-dialog',
  templateUrl: './alert-dialog.component.html',
  styleUrls: ['./alert-dialog.component.css']
})
export class AlertDialogComponent implements OnInit {
  title: string;
  message: string;
  constructor(private dialogRef: MatDialogRef<AlertDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: MessageData) {
    this.title = data.title;
    this.message = data.message;
  }
  public setMessage(title: string, message: string): void {
    this.title = title;
    this.message = message;
  }
  ngOnInit(): void {
  }

  close(): void {
    this.dialogRef.close();
  }
}
