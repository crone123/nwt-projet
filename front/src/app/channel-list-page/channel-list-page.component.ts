import {Component, EventEmitter, Input, OnInit} from '@angular/core';
import {ChannelItem} from '../../interfaces/channel-item';

@Component({
  selector: 'app-channel-list-page',
  templateUrl: './channel-list-page.component.html',
  styleUrls: ['./channel-list-page.component.css']
})
export class ChannelListPageComponent implements OnInit {
  @Input()
  public page: Array<ChannelItem>;
  @Input()
  refreshEmitter: EventEmitter<any>;
  constructor() { }
  ngOnInit(): void {
  }
}
