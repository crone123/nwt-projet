import {Component, EventEmitter, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {AddChannelDialogComponent} from '../add-channel-dialog/add-channel-dialog.component';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {
  dialogOpened: boolean;
  addChannel: EventEmitter<any>;
  refreshEmitter: EventEmitter<any>;
  constructor(public dialog: MatDialog) {
    this.dialogOpened = false;
    this.refreshEmitter = new EventEmitter<any>();
    this.addChannel = new EventEmitter<any>();
    this.addChannel.subscribe(_ =>
    {
      this.showDialog();
    });
  }

  ngOnInit(): void {
  }
  showDialog(): void {
    this.dialogOpened = true;
    console.log('Dialog opened !');
    const dialogRef = this.dialog.open(AddChannelDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      this.dialogOpened = false;
    });
    dialogRef.componentInstance.createSuccess.subscribe(_ => {
      this.refreshEmitter.emit();
    });
  }

}
