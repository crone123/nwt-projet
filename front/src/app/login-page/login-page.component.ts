import {Component, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {Data} from '../data';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {
  login: string;
  data: Data;
  constructor(private router: Router) {
    this.data = Data.getInstance();
  }

  ngOnInit(): void {
    this.login = '';
    if (this.data.isLoginOk())
    {
      this.router.navigate(['/main']);
    }
  }

  go(): boolean {
    console.log('Login: ' + this.login);
    if (this.login.length > 0)
    {
      Data.getInstance().setLogin(this.login);
      this.router.navigate(['/main']);
    }
    return false;
  }
  clear(): boolean {
    this.login = '';
    return false;
  }
}
