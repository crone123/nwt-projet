import {Component, EventEmitter, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {ChannelItem} from '../../interfaces/channel-item';
import {BACK_HTTP} from '../global';
import {HttpClient, HttpParams} from '@angular/common/http';
import {AlertDialogComponent} from '../alert-dialog/alert-dialog.component';

@Component({
  selector: 'app-add-channel-dialog',
  templateUrl: './add-channel-dialog.component.html',
  styleUrls: ['./add-channel-dialog.component.css']
})
export class AddChannelDialogComponent implements OnInit {
  name: string;
  public createSuccess: EventEmitter<any>;
  public createFailed: EventEmitter<any>;
  constructor(private dialogRef: MatDialogRef<AddChannelDialogComponent>, private client: HttpClient, public alert: MatDialog) {
    this.createSuccess = new EventEmitter<any>();
    this.createFailed = new EventEmitter<any>();
    this.name = '';
  }

  ngOnInit(): void {
  }
  close(): void
  {
    this.dialogRef.close();
  }

  create(): boolean {
    if (this.name.length === 0)
    {
      return false;
    }
    this.client.post<boolean>(BACK_HTTP + '/channels/create', new HttpParams().append('name', this.name)).subscribe(
      req => {
        // this.list =  req;
        console.log(req);
        // this.setPage(0);
        if (req === true)
        {
          this.createSuccess.emit();
          this.close();
        }
        else
        {
          this.createFailed.emit();
          const dialogRef = this.alert.open(AlertDialogComponent, {data: {title: 'Erreur de création de salon', message: 'Le serveur à refusé la demande.'}});
        }
      }
    );
    return false;
  }
}
