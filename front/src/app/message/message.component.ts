import {Component, EventEmitter, Input, OnInit} from '@angular/core';
import {Message} from '../../interfaces/message';
import {Data} from '../data';
import {HttpClient} from '@angular/common/http';
import {BACK_HTTP} from '../global';
import {AlertDialogComponent} from '../alert-dialog/alert-dialog.component';
import {MatDialog} from '@angular/material/dialog';
import {AddChannelDialogComponent} from '../add-channel-dialog/add-channel-dialog.component';
import {EditMessageDialogComponent} from '../edit-message-dialog/edit-message-dialog.component';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {
  @Input()
  message: Message;
  data: Data;

  dialogOpened: boolean;
  refreshEmitter: EventEmitter<any>;
  constructor(private client: HttpClient, public alert: MatDialog, public dialog: MatDialog) {
    this.data = Data.getInstance();

    this.dialogOpened = false;
    this.refreshEmitter = new EventEmitter<any>();
  }

  ngOnInit(): void {
  }
  timestampLimit(): number {
    const ts = Math.trunc(new Date().getTime() / 86400000) * 86400000;
    return ts;
  }
  // Si liké par notre utilisateur
  isLiked(): boolean
  {
    if (this.message.likes === undefined)
    {
      return false;
    }
    let c = 0;
    const login = this.data.getLogin();
    while (c < this.message.likes.length)
    {
      if (this.message.likes[c] === login)
      {
        return true;
      }
      c += 1;
    }
    return false;
  }
  likeColor(): string {
    return this.isLiked() ? 'accent' : '';
  }
  likeCount(): number {
    if (this.message.likes === undefined || this.message.likes === null)
    {
      return 0;
    }
    return this.message.likes.length;
  }
  likeBadge(): string {
    const nb = this.likeCount();
    if (nb === 0)
    {
      return '';
    }
    return '' + nb;
  }
  toggleLike(): void {
    if (this.isLiked())
    {
      this.unLike();
    }
    else
    {
      this.like();
    }
  }

  like(): void {

    this.client.post<boolean>(BACK_HTTP + '/messages/like', {
      idMessage: this.message.id,
      pseudo: this.data.getLogin(),
    }).subscribe(
      req => {
        if (req === false)
        {
          const alert = this.alert.open(AlertDialogComponent, {data: {title: 'Une erreur est survenue', message: 'le message n\'a pas pu être liké, veuillez ré-essayer.'}});
        }
      }
    );
    // Todo, like api call on messages
  }
  unLike(): void {
    this.client.post<boolean>(BACK_HTTP + '/messages/dislike', {
      idMessage: this.message.id,
      pseudo: this.data.getLogin(),
    }).subscribe(
      req => {
        if (req === false)
        {
          const alert = this.alert.open(AlertDialogComponent, {data: {title: 'Une erreur est survenue', message: 'le message n\'a pas pu être dé-liké, veuillez ré-essayer.'}});
        }
      }
    );
    // Todo, unlike api call on messages
  }

  updateMessage(): void {
    this.dialogOpened = true;
    // console.log('Dialog opened !');
    const dialogRef = this.dialog.open(EditMessageDialogComponent,{data: this.message, panelClass: 'custom-dialog-message'});
    dialogRef.updateSize('80%');
    dialogRef.afterClosed().subscribe(result => {
      this.dialogOpened = false;
    });
    /*
    dialogRef.componentInstance.createSuccess.subscribe(_ => {
      this.refreshEmitter.emit();
    });*/
  }
}
