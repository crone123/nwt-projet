import {EventEmitter, Injectable} from '@angular/core';
import {Socket} from 'ngx-socket-io';
import {Message} from '../../interfaces/message';

@Injectable()
export class MessageService
{
  receiveMsg: EventEmitter<Message>;
  receiveClose: EventEmitter<number>;
  constructor(private socket: Socket) {
    this.receiveMsg = new EventEmitter<Message>();
    this.receiveClose = new EventEmitter<number>();
    socket.on('message', msg => {
      this.receiveMsg.emit(msg);
    });
    socket.on('close', msg =>
    {
      this.receiveClose.emit(msg);
    });
  }
  public test(): void
  {
    this.socket.emit('test', ['test !']);
  }
}
