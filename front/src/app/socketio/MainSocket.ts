import {Injectable} from '@angular/core';
import {Socket} from 'ngx-socket-io';
import {BACK_IO} from '../global';

@Injectable()
export class MainSocket extends Socket {

  constructor() {
    super({ url: BACK_IO, options: {} });
    this.on('test', s => {
      console.log('Server test: ', s);
    });
  }

}
