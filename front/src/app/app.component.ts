import {Component, EventEmitter} from '@angular/core';
import {Data} from './data';
import {Router} from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public static disconnectEvent: EventEmitter<any> = new EventEmitter<any>();
  title = 'front';
  data: Data = Data.getInstance();
  constructor(private router: Router)
  {
    this.title = this.data.appName();
  }

  disconnect(): void
  {
    // console.log('Disconnect !');
    this.data.disconnect();
    AppComponent.disconnectEvent.emit();
    this.router.navigate(['/login']);
  }
}
