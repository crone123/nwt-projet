export class Data
{
  private static instance: Data;
  private login = '';
  private elementsByPage = 0;
  private constructor()
  {
    const login = localStorage.getItem('login');
    if (login == null)
    {
      this.login = '';
    }
    else
    {
      this.login = login;
    }
    const elementsByPage = localStorage.getItem('elementsByPage');
    if (elementsByPage == null)
    {
      this.elementsByPage = 25;
    }
    else
    {
      this.elementsByPage = Number(elementsByPage);
    }
  }
  public static getInstance(): Data
  {
    if (Data.instance == null)
    {
      Data.instance = new Data();
    }
    return Data.instance;
  }
  getLogin(): string
  {
    return this.login;
  }

  setLogin(login: string): void
  {
    this.login = login;
    localStorage.setItem('login', this.login);
  }
  isLoginOk(): boolean
  {
    return  this.login.length > 0;
  }
  appName(): string
  {
    return 'Angular Messenger';
  }

  disconnect(): void {
    this.setLogin('');
  }

  getElementsByPage(): number {
    return this.elementsByPage;
  }
  setElementsByPage(n: number): void  {
    this.elementsByPage = n;
    localStorage.setItem('elementsByPage', n.toString());
  }
  getElementsByPageAvailable(): Array<number> {
    return new Array(15, 20, 25, 50, 100);
  }
}
