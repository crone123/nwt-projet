import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {MatFormFieldModule} from '@angular/material/form-field';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatSliderModule} from '@angular/material/slider';
import {MatInputModule} from '@angular/material/input';
import {MatDialogModule} from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatListModule} from '@angular/material/list';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatCardModule} from '@angular/material/card';
import {MatBadgeModule} from '@angular/material/badge';
import {MatToolbarModule} from '@angular/material/toolbar';
import {Routes, RouterModule, Router} from '@angular/router';
import { LoginPageComponent } from './login-page/login-page.component';
import {AppRoutingModule} from './app-routing-module';
import { MainPageComponent } from './main-page/main-page.component';
import { DiscussionPageComponent } from './discussion-page/discussion-page.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ChannelListComponent } from './channel-list/channel-list.component';
import { ChannelItemComponent } from './channel-item/channel-item.component';
import { AddChannelDialogComponent } from './add-channel-dialog/add-channel-dialog.component';
import {MatSelectModule} from '@angular/material/select';
import { ChannelListPageComponent } from './channel-list-page/channel-list-page.component';
import {MatPaginatorModule} from '@angular/material/paginator';
import { MessageComponent } from './message/message.component';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import { HttpClientModule } from '@angular/common/http';
import { AlertDialogComponent } from './alert-dialog/alert-dialog.component';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import {BACK_IO} from './global';
import {MainSocket} from './socketio/MainSocket';
import { EditMessageDialogComponent } from './edit-message-dialog/edit-message-dialog.component';
import { RemoveChannelDialogComponent } from './remove-channel-dialog/remove-channel-dialog.component';

const config: SocketIoConfig = { url: BACK_IO, options: {} };
@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    MainPageComponent,
    DiscussionPageComponent,
    ChannelListComponent,
    ChannelItemComponent,
    AddChannelDialogComponent,
    ChannelListPageComponent,
    MessageComponent,
    AlertDialogComponent,
    EditMessageDialogComponent,
    RemoveChannelDialogComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatSliderModule,
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule,
    MatButtonModule,
    MatIconModule,
    MatListModule,
    MatProgressSpinnerModule,
    MatCardModule,
    MatBadgeModule,
    MatToolbarModule,
    RouterModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatPaginatorModule,
    MatSlideToggleModule,
    HttpClientModule,
    SocketIoModule.forRoot(config),
  ],
  providers: [MainSocket],
  bootstrap: [AppComponent]
})


export class AppModule
{
}
