import {Component, EventEmitter, Inject, OnInit} from '@angular/core';
import {BACK_HTTP} from '../global';
import {AlertDialogComponent, MessageData} from '../alert-dialog/alert-dialog.component';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Message} from '../../interfaces/message';

@Component({
  selector: 'app-edit-message-dialog',
  templateUrl: './edit-message-dialog.component.html',
  styleUrls: ['./edit-message-dialog.component.css']
})
export class EditMessageDialogComponent implements OnInit {
  message: string;

  public createSuccess: EventEmitter<any>;
  public createFailed: EventEmitter<any>;

  // tslint:disable-next-line:max-line-length
  constructor(private dialogRef: MatDialogRef<EditMessageDialogComponent>, private client: HttpClient, public alert: MatDialog, @Inject(MAT_DIALOG_DATA) public data: Message) {
    this.createSuccess = new EventEmitter<any>();
    this.createFailed = new EventEmitter<any>();
    this.message = data.message;
  }

  ngOnInit(): void {
  }
  close(): void
  {
    this.dialogRef.close();
  }

  update(): boolean {

    if (this.message.length === 0)
    {
      return false;
    }
    this.data.message = this.message;
    this.client.post<boolean>(BACK_HTTP + '/messages/update', {

      id: this.data.id,
      likes: this.data.likes,
      pseudo: this.data.pseudo,
      message: this.message,
      timestamp: this.data.timestamp,
      channel: this.data.channel,
      }).subscribe(
      req => {
        if (req === false)
        {
          const alert = this.alert.open(AlertDialogComponent, {data: {title: 'Une erreur est survenue', message: 'le message n\'a pas pu être dé-liké, veuillez ré-essayer.'}});
        }else {
          this.close();
        }
      }
    );

    return false;
    }

}
