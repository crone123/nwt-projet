import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Data} from '../data';
import {Message} from '../../interfaces/message';
import {ChannelItem} from '../../interfaces/channel-item';
import {BACK_HTTP} from '../global';
import {HttpClient} from '@angular/common/http';
import {MatDialog} from '@angular/material/dialog';
import {AlertDialogComponent} from '../alert-dialog/alert-dialog.component';
import {MessageService} from '../socketio/message-service';
import {Socket} from 'ngx-socket-io';
import {Subscription} from 'rxjs';
import {AppComponent} from '../app.component';

@Component({
  selector: 'app-discussion-page',
  templateUrl: './discussion-page.component.html',
  styleUrls: ['./discussion-page.component.css']
})
export class DiscussionPageComponent implements OnInit, OnDestroy {
  id: number;
  data: Data;
  messages: Array<Message>;
  message: string;
  channel: ChannelItem;
  ms: MessageService;
  autoScrollEnabled: boolean;
  subscriptionId: Subscription;
  subscriptionMsg: Subscription;
  subscriptionClose: Subscription;
  constructor(private route: ActivatedRoute, private router: Router, private client: HttpClient, public alert: MatDialog, private socket: Socket) {
    this.data = Data.getInstance();
    this.messages = null;
    this.message = '';
    this.ms = new MessageService(socket);
    this.autoScrollEnabled = true;
  }
  unsubscribe(): void {
    if (this.subscriptionId !== undefined && this.subscriptionId != null)
    {
      // console.log('Unsubscribe !');
      this.subscriptionId.unsubscribe();
      this.subscriptionId = null;
    }
    if (this.subscriptionMsg !== undefined && this.subscriptionMsg != null)
    {
      // console.log('Unsubscribe msg !');
      this.subscriptionMsg.unsubscribe();
      this.subscriptionMsg = null;
    }
    if (this.subscriptionClose !== undefined && this.subscriptionClose != null)
    {
      this.subscriptionClose.unsubscribe();
      this.subscriptionClose = null;
    }
  }
  ngOnDestroy(): void {
        this.unsubscribe();
    }

  ngOnInit(): void {
    this.updateId();
    AppComponent.disconnectEvent.subscribe(_ => {
      this.unsubscribe();
    });
    this.subscriptionId = this.router.events.subscribe(obj =>
    {
      if (this !== null && this !== undefined)
      {
        this.updateId();
      }
    });
    this.subscriptionMsg = this.ms.receiveMsg.subscribe(msg => {
      if (this !== null && this !== undefined)
      {
        this.liveReceiveMessage(msg);
      }
    });
    this.subscriptionClose = this.ms.receiveClose.subscribe(channelId => {
      console.log('Channel closed: ' + Number(channelId));
      console.log('Current channel: ' + this.id);
      if (Number(channelId) === this.id)
      {
        console.log('We will close.');
        this.close();
      }
    });
    // this.ms.test(); // Test message to the server
  }
  updateId(): void {
    this.id = Number(this.route.snapshot.paramMap.get('id'));
    if (!this.data.isLoginOk())
    {
      this.router.navigate(['/login']);
    }
    else if (this.id == null || this.id === 0)
    {
      this.router.navigate(['/main']);
    }
    else
    {
      this.getChannel();
    }
  }
  getChannel(): void
  {
    this.messages = null;
    this.client.get<ChannelItem>(BACK_HTTP + '/channels/get/' + this.id).subscribe(
      req => {
        this.channel =  req;
        // console.log(req);
        if (this.channel == null)
        {
          this.router.navigate(['/main']);
        }
        else
        {
          this.refreshList();
        }
      }
    );
  }
  refreshList(): void
  {
    this.messages = null;
    this.client.get<Message[]>(BACK_HTTP + '/messages/list/' + this.id).subscribe(
      req => {
        this.messages =  req;
        // console.log(req);
        this.setFocus();
        setTimeout(_ => {this.scrollDown(false); }, 100);
      }
    );
  }
  sendMessage(): boolean
  {
    if (this.message.length === 0)
    {
      return false;
    }
    this.client.post<boolean>(BACK_HTTP + '/messages/send', {
      channel: this.id,
      message: this.message,
      pseudo: this.data.getLogin(),
    }).subscribe(
      req => {
        if (req === true)
        {
          this.message = '';
          this.setFocus();
        }
        else
        {
          const alert = this.alert.open(AlertDialogComponent, {data: {title: 'Une erreur est survenue', message: 'Votre message n\'a pas pu être envoyé, veuillez ré-essayer.'}});
        }
      }
    );
    return false;
  }
  setFocus(): void
  {
    const input = document.getElementById('message_input');
    if (input !== null)
    {
      input.focus();
    }
  }
  liveReceiveMessage(msg: Message): void {
    // console.log('Message: ' + msg);
    if (msg.channel === this.id)
    {
      let found = false;
      let c = 0;
      let insertPosition = 0;
      let willScroll = false;
      while (c < this.messages.length)
      {
        if (this.messages[c].id === msg.id)
        {
          this.messages[c] = msg;
          found = true;
          break;
        }
        else if (this.messages[c].timestamp < msg.timestamp)
        {
          insertPosition = c + 1;
        }
        c += 1;
      }
      if (!found)
      {
        // console.log('insert: ' + insertPosition + ', length: ' + this.messages.length);
        if (insertPosition === this.messages.length)
        {
          this.messages.push(msg);
          willScroll = true;
        }
        else
        {
          const na = new Array<Message>();
          let toInsert = 0;
          while (na.length !== this.messages.length + 1)
          {
            if (na.length === insertPosition)
            {
              na.push(msg);
            }
            else
            {
              na.push(this.messages[toInsert]);
              toInsert += 1;
            }
          }
          this.messages = na;
        }
      }
      if (willScroll === true)
      {
        setTimeout(_ => {this.scrollDown(true); }, 100);
      }
    }
  }
  scrollUp(auto: boolean): void {
    if (!auto || auto && this.autoScrollEnabled)
    {
      const s = document.getElementById('discussion_page_scroll');
      if (s != null)
      {
        s.scroll({top: 0, behavior: 'smooth'});
      }
    }
  }
  scrollDown(auto: boolean): void {
    if (!auto || auto && this.autoScrollEnabled)
    {
      const s = document.getElementById('discussion_page_scroll');
      if (s != null)
      {
        s.scroll({top: s.scrollHeight, behavior: 'smooth'});
      }
    }
  }
  toggleAutoScroll(): void {
    this.autoScrollEnabled = !this.autoScrollEnabled;
  }
  close(): void
  {
    console.log('In close');
    this.unsubscribe();
    console.log('Unsubscribed !');
    const alert = this.alert.open(AlertDialogComponent, {data: {title: 'Le salon à été fermé', message: 'Pas de panique, nous vous ramenons dans le hall !'}});
    console.log('After alert !');
    alert.afterClosed().subscribe(_ => {
      console.log('Redirect to main !');
      this.router.navigate(['/main']);
    });
  }
}
