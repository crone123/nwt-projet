import {Component, EventEmitter, Injectable, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Data} from '../data';
import {ChannelItem} from '../../interfaces/channel-item';
import {PageEvent} from '@angular/material/paginator';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {BACK_HTTP} from '../global';

@Component({
  selector: 'app-channel-list',
  templateUrl: './channel-list.component.html',
  styleUrls: ['./channel-list.component.css']
})

@Injectable()
export class ChannelListComponent implements OnInit {
  @Input()
  refreshEmitter: EventEmitter<any>;
  @Input()
  addChannel: EventEmitter<any>;
  list: Array<ChannelItem>;
  currentPage: number;
  page: Array<ChannelItem>;
  pageList: Array<number>;
  data: Data;
  constructor(private router: Router, private client: HttpClient) {
    // this.list = new Array<ChannelItem>();
    this.list = null;
    this.data = Data.getInstance();
    this.refreshList();
  }

  ngOnInit(): void {
    if (!this.data.isLoginOk())
    {
      this.router.navigate(['/login']);
    }
    this.refreshEmitter.subscribe(_ => {
      this.refreshList();
    });
  }
  refreshList(): void
  {
    this.list = null;
    this.client.get<ChannelItem[]>(BACK_HTTP + '/channels/list').subscribe(
      req => {
        this.list =  req;
        // console.log(req);
        this.setPage(0);
      }
    );
  }
  // tslint:disable-next-line:typedef
  setPage(page: number) {
    this.currentPage = page;
    const elementsByPage = this.data.getElementsByPage();
    let c = this.currentPage * elementsByPage;
    const cm = (this.currentPage + 1) * elementsByPage;
    this.page = new Array();
    while (c < this.list.length && c < cm)
    {
      this.page.push(this.list[c]);
      c += 1;
    }
  }


  updatePage(pageEvent: PageEvent): void {
    this.data.setElementsByPage(pageEvent.pageSize);
    this.setPage(pageEvent.pageIndex);
  }
  showDialog(): void {
    this.addChannel.emit();
  }
}
