import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginPageComponent} from './login-page/login-page.component';
import {MainPageComponent} from './main-page/main-page.component';
import {DiscussionPageComponent} from './discussion-page/discussion-page.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginPageComponent },
  {path: 'main', component: MainPageComponent},
  {path: 'channel/:id', component: DiscussionPageComponent},
  {path: 'channel', component: DiscussionPageComponent},
 /* { path: 'people', component: PeopleComponent },
  { path: 'edit/:id', component: UpdateComponent }*/
];

@NgModule({
  imports: [ RouterModule.forRoot(routes, { useHash: true }) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {
}
