# Projet de nouvelles technologies du web

- Valentin CRÔNE - Damien LEVY

## Installation

Pour que le projet fonctionne, il vous faut nodejs et yarn.

### One step

```
./run.sh
```

Ce script déploie la base mongo, installe les dépendances du back et front avec yarn, et lance le back et front.
Il suffit ensuite d'accéder à:
- http://localhost:4200 (front)
- http://localhost:3000 (back, swagger)


Pour arrêter les services:

```
./stop.sh
```

### Step by step

- Déployer le conteneur mongo:

```
cd back
docker-compose up
```

- Démarrer le back:

```
cd back
yarn install
yarn run start
```


- Démarrer le front:

```
cd front
yarn install
yarn run start
```

- Accéder a l'interface web (lien ci-dessus)

- Terminer les services (ctrl+C)
